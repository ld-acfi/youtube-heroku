<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class YoutubeControlelrController extends AbstractController
{
    /**
     * @Route("/youtube/controlelr", name="youtube_controlelr")
     */
    public function index(): Response
    {
        return $this->render('youtube_controlelr/index.html.twig', [
            'controller_name' => 'YoutubeControlelrController',
        ]);
    }
}
